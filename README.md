# Management Cluster LCM

This is a set of ansible tasks layered on top of
[yaook/k8s](https://github.com/yaook/k8s). They manage the services to deploy
in order to get a working Cluster Management Cluster.

The following services are get deployed:

- HashiCorp Vault
- Certmanager
- DNSmasq
- Ironic
- Netbox
